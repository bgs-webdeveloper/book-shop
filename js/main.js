//Slider
$(document).ready(function(){
  $('.slider-products').slick({
      infinite: true,
      slidesToShow: 6,
      slidesToScroll: 1,
      arrows: true,
      responsive: [
        {
          breakpoint: 1440,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 1,
            infinite: true,
          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        }
      ]
  });
    
  $('.slider-banner').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: true
  });
});

function openCatalog(menuCatalog) {
    var element = document.getElementById(menuCatalog);
    var btn = document.getElementById('btn-toogle-menu-mobile');
    
    if(element.classList.contains('show')){
       element.classList.remove('show');
       btn.firstElementChild.classList.remove('close-icon');
       if(window.innerWidth < 576){
           document.body.style.overflow = 'auto';
       }
    } else {
       element.classList.add('show');
       btn.firstElementChild.classList.add('close-icon');
       if(window.innerWidth < 576){
       document.body.style.overflow = 'hidden';
       }
    }
}

function starsOn(stars, event) {
    var target = event.target,
        starsValue;

    if(target.tagName == 'I'){
        
        if(target.classList.contains('fas')){
            //Цикл удаляющий классы 'active' ко всем 'LI' до выбранной зведзы
            for(var i=stars.children.length - 1; i >= 0; i--){ 
                if(stars.children[i].classList.contains('fas')){
                    stars.children[i].classList.remove('fas');
                    stars.children[i].classList.add('far');
                }

                if(stars.children[i] == target){
                    stars.children[i].classList.add('fas');
                    starsValue = i + 1;
                    /*console.log('1');*/
                    break;
                }
            }//end for

        }else{
            //Цикл добавляющий классы 'active' ко всем 'LI' до выбранной зведзы
            for(var i=0; i < stars.children.length; ++i){
                stars.children[i].classList.remove('far');
                stars.children[i].classList.add('fas');
                if(stars.children[i] == target){
                    starsValue = i + 1;
                    break;
                }
            }//end for

        } 
    }   
    stars.parentNode.childNodes[3].value = starsValue;
};

function btnMore(btn){
    $(btn).siblings('.part-of-the-content').addClass('more-text');
}

function transformRotateCkevron(element){
    if(!element.classList.contains('opened')){
        element.classList.add('opened');
    } else {
        element.classList.remove('opened');
    }
}

function showFilter(btn) {
    if(!document.getElementById('filter-block').classList.contains('show')){
        document.getElementById('filter-block').classList.add('show');
    } else {
        document.getElementById('filter-block').classList.remove('show');
    }
    
}


//Validation email
function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function validate() {
  /*var result = $("#result-valid");*/
  var email = $("#email").val();
  /*result.text("");*/

  if (validateEmail(email)) {
    /*result.text(email + " is valid :)");
    result.css("color", "green");*/
    /*$("#email").css('background-color', '#23fd63');*/
    $("#email").css('color', '#0cd045');
    $("#email").val(email + ' is valid');
  } else {
    /*result.text(email + " is not valid :(");
    result.css("color", "red");*/
    /*$("#email").css('background-color', '#ff6464');*/
    $("#email").css('color', '#ff6464');
    $("#email").val(email + ' is not valid');
  }
  return false;
}
//Validation email end
$("#validate").bind("click", validate);

function sortToList(btn){
    if(!btn.classList.contains('active')){
        btn.classList.add('active');
        btn.nextElementSibling.classList.remove('active');
        $('.products-cards .product-card').addClass('list');
    }
}

function sortToGrid(btn){
    if(!btn.classList.contains('active')){
        btn.classList.add('active');
        btn.previousElementSibling.classList.remove('active');
        $('.products-cards .product-card').removeClass('list');
    }
}

function clickOnProduct(product){
    if(window.innerWidth < 991){
        $(".product").find('.bondage').removeClass('hide');
        $(".product").find('.product-hover').removeClass('show-flex');
        $(product).find('.title-product').removeClass('blue');

        $(product).find('.bondage').addClass('hide');
        $(product).find('.product-hover').addClass('show-flex');
        $(product).find('.title-product').addClass('blue');
    }
}

function switchingBuyDelivary(btn){
    if(btn === document.getElementById('your-product')){
        document.getElementById('shipping-and-payment').classList.remove('active');
        document.getElementById('switch-delivary').classList.remove('active');
        document.getElementById('your-product').classList.add('active');
        document.getElementById('switch-buy').classList.add('active');
        
    } else {
        document.getElementById('your-product').classList.remove('active');
        document.getElementById('switch-buy').classList.remove('active');
        document.getElementById('shipping-and-payment').classList.add('active');
        document.getElementById('switch-delivary').classList.add('active');
    }
}

if(document.getElementById('onscrolling')){
    window.onscroll = function() {
        var startScrolling = document.getElementById('onscrolling').getBoundingClientRect().bottom;
        var endScrolling = document.getElementById('nav-tabContent').getBoundingClientRect().bottom;
        var elementScrolling = document.getElementById('element-for-scrolling').getBoundingClientRect().height;
        
        if(startScrolling < 20 && endScrolling > 20 + elementScrolling){
            document.getElementById('element-for-scrolling').classList.add('fixed');
        } else {
            document.getElementById('element-for-scrolling').classList.remove('fixed');
        }
    }
}


function catSortAll(btn){
    btn.parentElement.parentElement.parentElement.classList.add('select-block');
    
    for( var i = 0; i < btn.parentElement.children.length; i++ ) {
        btn.parentElement.children[i].classList.remove('active');
    }
    
    btn.classList.add('active');
    
    for( var i = 0; i < $('.select-block .products-cat-sort .product-card').length; i++ ) {
        $('.select-block  .products-cat-sort .product-card')[i].classList.remove('hide');
    }
    
    btn.parentElement.parentElement.parentElement.classList.remove('select-block');
}
function catSort(btn, cat) {
    btn.parentElement.parentElement.parentElement.classList.add('select-block');
    $('.select-block .products-cat-sort .product-card').addClass('hide');
    
    for( var i = 0; i < btn.parentElement.children.length; i++ ) {
        btn.parentElement.children[i].classList.remove('active');
    }
    
    
    btn.classList.add('active');
    
    for(var i = 0; i < $('.select-block .products-cat-sort .product-card').length; i++){
        if( $('.select-block .products-cat-sort .product-card')[i].classList.contains(cat) ) {
            $('.select-block .products-cat-sort .product-card')[i].classList.remove('hide');
        }
    }
    btn.parentElement.parentElement.parentElement.classList.remove('select-block');
}

if(window.innerWidth <= 575){
    $('.products-slider').slick({
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      infinite: false,
      arrows: false
    });
}

function showSearchMobile(){
    var searchBlock = document.getElementById('search-mobile-block')
    
    if(searchBlock.classList.contains('show-serch-block')){
        searchBlock.classList.remove('show-serch-block');
    } else {
        searchBlock.classList.add('show-serch-block');
    }   
}
//Show mobile catalog books
/*if(document.getElementById('sidebar-mobile').classList.contains('show')){
    console.log('true');
    var catalogHeight = document.getElementById('catalog-mobile').getBoundingClientRect().height;
        console.log(catalogHeight);
        document.getElementById('catalog-mobile').style.marginTop = '-' + catalogHeight + 'px';
}*/

function showMobileCatalog(){
    document.getElementById('catalog-mobile').classList.toggle('show');
    document.getElementById('arrow-to-mobile-catalog').classList.toggle('list-arrow-bottom')
}

function minusCount(btn){
    var input = btn.nextElementSibling;
    if(input.value > 1){
        input.value = parseFloat(input.value) - 1;
    }
}

function plusCount(btn){    
    var input = btn.previousElementSibling;
        input.value = parseFloat(input.value) + 1;
}